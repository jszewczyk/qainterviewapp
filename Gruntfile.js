module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    'http-server': {
      'template': {
        // the server root directory
        root: 'Template/',
        // the server port
        // can also be written as a function, e.g.
        // port: function() { return 8282; }
        port: 8282,

        // the host ip address
        // If specified to, for example, "127.0.0.1" the server will
        // only be available on that ip.
        // Specify "0.0.0.0" to be available everywhere
        host: "127.0.0.1",

        showDir : true,
        autoIndex: true,

        // server default file extension
        ext: "html",

        // run in parallel with other tasks
        runInBackground: false,
      },
      'dev': {
        // the server root directory
        root: 'src/public/',
        // the server port
        // can also be written as a function, e.g.
        // port: function() { return 8282; }
        port: 8282,

        // the host ip address
        // If specified to, for example, "127.0.0.1" the server will
        // only be available on that ip.
        // Specify "0.0.0.0" to be available everywhere
        host: "127.0.0.1",

        showDir : true,
        autoIndex: true,

        // server default file extension
        ext: "html",

        // run in parallel with other tasks
        runInBackground: false,
      }

    }
  });


  grunt.loadNpmTasks('grunt-http-server');
};