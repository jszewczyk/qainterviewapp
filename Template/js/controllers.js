/**
 * Created by Sergey on 24.02.2016.
 */

(function() {

    lendProject .controller('mainCtrl', mainCtrl)
        .controller('datePickerCtrl', datePickerCtrl)
        .controller('timePickerCtrl', timePickerCtrl)
        .controller('getLocationsCtrl',getLocationsCtrl)
        .controller('viewCarDetailsCtrl', viewCarDetailsCtrl)
        .controller('viewCarInfoCtrl', viewCarInfoCtrl);

    function mainCtrl($scope, $log, $stateParams, $http) {

        // *****************************************List of data ****************************************************
        $scope.cars = [
            {
                id:1,
                image: 'img/cars/car1.jpg',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:2,
                image: 'img/cars/car2.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:3,
                image: 'img/cars/car3.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:4,
                image: 'img/cars/car4.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:5,
                image: 'img/cars/car5.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:6,
                image: 'img/cars/car6.jpg',
                model: 'Range Rover',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:7,
                image: 'img/cars/car7.png',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:8,
                image: 'img/cars/car8.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:9,
                image: 'img/cars/car9.png',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:10,
                image: 'img/cars/car10.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:11,
                image: 'img/cars/car11.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:12,
                image: 'img/cars/car12.jpg',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:13,
                image: 'img/cars/car13.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:14,
                image: 'img/cars/car14.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:15,
                image: 'img/cars/car13.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:16,
                image: 'img/cars/car12.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:17,
                image: 'img/cars/car11.jpg',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:18,
                image: 'img/cars/car12.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:19,
                image: 'img/cars/car13.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:20,
                image: 'img/cars/car10.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:21,
                image: 'img/cars/car9.png',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:22,
                image: 'img/cars/car12.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:23,
                image: 'img/cars/car13.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:24,
                image: 'img/cars/car14.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:25,
                image: 'img/cars/car5.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:26,
                image: 'img/cars/car6.jpg',
                model: 'Range Rover',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:27,
                image: 'img/cars/car1.jpg',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:28,
                image: 'img/cars/car2.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:29,
                image: 'img/cars/car3.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:30,
                image: 'img/cars/car4.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:31,
                image: 'img/cars/car5.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:32,
                image: 'img/cars/car10.jpg',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:33,
                image: 'img/cars/car11.jpg',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:34,
                image: 'img/cars/car12.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:35,
                image: 'img/cars/car13.jpg',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:36,
                image: 'img/cars/car14.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:37,
                image: 'img/cars/car6.jpg',
                model: 'Audi',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:38,
                image: 'img/cars/car7.png',
                model: 'Honda',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:39,
                image: 'img/cars/car8.jpg',
                model: 'Maseratti',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:40,
                image: 'img/cars/car9.png',
                model: 'Opel',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            },
            {
                id:41,
                image: 'img/cars/car10.jpg',
                model: 'VW',
                description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
                'Now, one is for sale in the Middle East at Topline Motors'
            }
        ];

        $scope.totalItems = $scope.cars.length;
        $scope.currentPage = 1;
        $scope.maxSize = 9;


        $scope.setPage = function (pageNumber) {
            $scope.currentPage = pageNumber;
        };

        $scope.$watch('currentPage', function (num) {
            var begin = (($scope.currentPage - 1) * $scope.maxSize),
                end = begin + $scope.maxSize;

            $scope.filteredCars = $scope.cars.slice(begin, end);
        });
        if($stateParams.carId !== undefined){
            $scope.car = $scope.cars[$stateParams.carId];
        }
    }

    function datePickerCtrl($scope) {

        $scope.today = function() {
            $scope.dt = new Date();
        };

        $scope.today();                 // on button click Today - shows today's date on calendar
        $scope.clear = function() {     // on button click Clear - clears the selected date
            $scope.dt = null;
        };

        $scope.dateOptions = {
            maxDate: new Date(2025, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        $scope.open = function() {
            $scope.popup.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        $scope.popup = {
            opened: false
        };
    }

    function timePickerCtrl($scope, $http) {

        $scope.selectedTime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 1;
    }

    function getLocationsCtrl($scope, $http){

        var _selected;

        $scope.selected = undefined;

        $scope.getLocation = function(val) {
            return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function(response){
                return response.data.results.map(function(item){
                    return item.formatted_address;
                });
            });
        };

        $scope.ngModelOptionsSelected = function(value) {
            if (arguments.length) {
                _selected = value;
            } else {
                return _selected;
            }
        };

        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
    }

    function viewCarDetailsCtrl($scope, $stateParams){

        // Display car details - image, name, description
        if($stateParams.carId !== undefined){
            $scope.car = $scope.cars[$stateParams.carId];
        }

        // SLIDER CONTROLLER
        $scope.myInterval = 4000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        var slides = $scope.slides = [];
        var currIndex = 0;

        $scope.addSlide = function() {
            var newWidth = 600 + slides.length + 1;
            slides.push({
                image: 'http://lorempixel.com/' + newWidth + '/300',
                text: ['Nice image','Awesome photograph','That is so cool','I love that'][slides.length % 4],
                id: currIndex++
            });
        };

        $scope.randomize = function() {
            var indexes = generateIndexesArray();
            assignNewIndexesToSlides(indexes);
        };

        for (var i = 0; i < 6; i++) {
            $scope.addSlide();
        }

        // Randomize logic below
        function assignNewIndexesToSlides(indexes) {
            for (var i = 0, l = slides.length; i < l; i++) {
                slides[i].id = indexes.pop();
            }
        }

        function generateIndexesArray() {
            var indexes = [];
            for (var i = 0; i < currIndex; ++i) {
                indexes[i] = i;
            }
            return shuffle(indexes);
        }

        function shuffle(array) {
            var tmp, current, top = array.length;
            if (top) {
                while (--top) {
                    current = Math.floor(Math.random() * (top + 1));
                    tmp = array[current];
                    array[current] = array[top];
                    array[top] = tmp;
                }
            }
            return array;
        }
    }

    function viewCarInfoCtrl($scope, $stateParams){
        if($stateParams.carId !== undefined){
            $scope.car = $scope.cars[$stateParams.carId];
        }
    };


})();