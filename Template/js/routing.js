/**
 * Created by Sergey on 24.02.2016.
 */

lendProject.config(function($stateProvider, $urlRouterProvider){
    $stateProvider

        .state('auth', {
            abstract: true,
            url: '/auth',
            templateUrl: "auth.html"
        })
        .state('auth.signin', {
            parent: "auth",
            url: "/signin",
            templateUrl: "signin.html"
        })
        .state('auth.register', {
            parent: "auth",
            url: "/register",
            templateUrl: "register.html"
        })

        .state('home', {
            abstract: true,
            url: "/",
            templateUrl: "home.html"
        })
        .state('home.main', {
            parent: "home",
            url: "",
            templateUrl: "main.html",
            controller: "mainCtrl"
        })
		.state('home.main.content', {
            parent: "home.main",
            url: "",
            templateUrl: "showResults.html",
            controller: "mainCtrl"
        })
        .state('home.main.carDetails', {
            parent: "home",
            url: "details/:carId",
            templateUrl: "carDetails.html",
            controller: "viewCarDetailsCtrl",
            params: {
                carId: undefined
            }
        })
        .state('home.main.carInfo', {
            parent: "home",
            url: "info/:carId",
            templateUrl: "carInfo.html",
            controller: "viewCarInfoCtrl",
            params: {
                carId: undefined
            }
        })

        .state('home.contacts', {
            parent: "home",
            url: "contacts",
            templateUrl: "contacts.html"
        })
        .state('home.deals', {
            parent: "home",
            url: "deals",
            templateUrl: "deals.html"
        })
        .state('results', {
            parent: "home",
            url: "results",
            templateUrl: "showResults.html",
            controller: "mainCtrl"
        })


        .state('admin', {
            url: "/admin",
            templateUrl: "adminPanel/adminIndex.html",
            controller: "mainCtrl"
        });

    $urlRouterProvider.otherwise('/auth/signin');
});
