# Interview questions for QA candidates

<!-- MarkdownTOC -->

- [General QA](#general-qa)
	- [Junior level](#junior-level)
	- [Regular level](#regular-level)
	- [Senior level](#senior-level)
- [ISTQB](#istqb)
- [Web technologies](#web-technologies)
- [Automation](#automation)
	- [Selenium](#selenium)
- [Technical questions](#technical-questions)
	- [Programming (general)](#programming-general)
	- [Python](#python)
- [Technology stack](#technology-stack)
- [Other](#other)

<!-- /MarkdownTOC -->

## General QA

### Junior level

* [J] What is a bug?
* [J] What information shall be delivered in bug report?
* [J] Who can report a bug?
* [J] Reported bug life cycle? Why we reports bugs in bug tracking system?
* [J] What are smoke tests / regression tests?
* [J] Describe a difference between testing and debugging?
* [J] Does developer have to test his own work? What type of testing?

### Regular level

* [R] What is the difference between *'Severity'* and *'Priority'*. Who is responsible for setting them?
* [R] What are test execution exit criteria? Give two examples of exit criteria from test execution process?
* [R] What data do you need to create test case?
* [R] What data do you put into test case?
* [R] What is a difference between Critical bug and Blocker bug?
* [R] What is responsible for delivered software quality?
* [R] What types of methods developers can introduce to improve quality?
* [R] Who defines Acceptance Test set?
* [R] If delivered SW has some extra functionality - do you consider this as an issue?
* [R] Does low performance solution, which meets all requirements, is an issue?
* [R] What is traceability? Give some examples in QA process
* [R] What is destructive testing? How QA can help in creating SW with is 'user' proof?
* [R] How do you measure time of taking photo on mobile device. Do you need support from developers?
* [R] What can be the reason, when same SW version on two same HW behaves differently?
* [R] How you can measure efficiency/performance of QA engineer.

### Senior level

* [S] What and how test cases do you select to execute when:
    * Developer make change based on CR
    * When target HW platform changes
    * When Client decide to use same HW, but from different supplier
* [S] How we can prove to Client, that delivered software is in expected quality?
* [S] Why we should monitor regressions in project?
* [S] What are mocks?


## ISTQB

* What is a difference between validation and verification?
* What is a difference between White Box and Black Box testing
* What is boundary value analysis? Give some examples.
* What is decision coverage?
* What is branch coverage?

## Web technologies

* Web page
	* What is the structure of `html` file?
	* What is an AJAX?
	* What is routing?
* CSS
	* What is a difference between selector `div .classname` and `div > .classname`
	* How to create CSS selector to element with custom attributes (e.g. id and class not defined)
* JavaScript
	* How to print a text on JavaScript console?
	* How to select an element in jQuery?

## Automation

* Entry criteria (prepare environment, setup configuration, generate/read test data)
* Clean up environment after script execution?


### Selenium

* What methods of html elements selectors do you know?
* How to select element from `<select>` element?
* What can you say about page object pattern?
* How use page object pattern in "Single Page Applications"?
* How to make Selenium test to wait for all async/AJAX connections with server to be closed?
* How to enter data to hidden `<input>` element?


## Technical questions

* What is the result of some logic equation? ((1 != 2) || (3 > 4)) && ((1 > 2) && (2 < 1))


### Programming (general)

* Create function which takes Integer , and returns
    * 'A' when provided integer divides by 3 without rest
    * 'B' when provided integer divides by 5 without rest
    * 'AB' when provided integer divides by 3 and 5 without rest

* Create function which prints Fibonacci sequence

* Count all occurrences for values in table ['a', 'b', 'a', 'c' ....]


### Python

* What following code do:

```python
def printAbFunction(x):

    if (x%3==0):
        print("a")
    if (x%5==0):
        print("b")

    if (x%3==0 and x%5 ==0 ):
        print ("ab")

*************************************************************************************
def showIt():
    for i in range(0,10):
        for k in range(0 , i):
            print "*",
        print "'"

*************************************************************************************
def fun(n):
a,b = 1,1
for i in range(n-1):
  a,b = b,a+b
  print a, b
return a
```



## Technology stack

Only to check if candidate was using particular technologies during his career
* Do you have used Git?
	* What is the chain of commands to make a commit and upload in on external git repository?
* Linux
	* How to store command output in a file?
	* How to search for a string in a file?
	* How to filter command output?
	* What types of permission you can set for a file?
* Cucumber
	* What is a `Feature`?
	* What is `Scenario Outline`?
	* What are data tables?
* Do you have experience with network protocol analysers?
* Do you know any script language? (python, ruby)

## SQL
1. How to combine rows from two or more tables?
   Answer: Joins

2. How to delete table?
   DROP TABLE

3. How to create table?
   CREATE TABLE

4. What doest the following query do?
   DELETE FROM People
   WHERE Name='Name' AND Surname='Surname';

5. We have a table People consist four columns: Name, Surname, Age, Address details. Assume the table includes 1000 records sorted by Surname.
   How to pick 5 records consist of oldest people?
   Answer:
   SELECT TOP 5 * FROM People
   ORDER BY Age;	



## Other
* When candidate can join our company
* Salary expectations



Junior - 4000 brutton
Regular - 6000 brutto
