1. Imagine situation: Developer is presenting you on his PC fix for ticket which you logged some time ago.  This bug is running correctly in all usecases on this presentation. Because bug is fixed developers ask you to change status of this tiket to close. What do you do: 
    * close the tiket
    * not close tiket

2. System is presenting uploaded pictures. 
It support: multiple file uploading and support also different picture type JPEG, RAW, GIF. 
Which test you consider as smoke tests: 
    * Upload 10 pictures in 1 minute one by one and check if last one is correctly presented.
    * Upload 3 pictures at once and check if all 3 are presented.
    * Upload one Jpeg picture and check if it is pretend
    * Upload giff and check if it is animated
    
3. What value will be as a result of following sentence: 
        (( 3 < 1 ) AND ( 1 != 2)) OR ((2>1) OR (1=0))
    * True 
    * False 
    * True and False 
    * Unpredictable 
4. Which elements from following are mandatory in bug report? 
    *  Time spend on testing 
    * Author 
    * Test Steps 
    * Priority 
    * SW version 
    * Tester ID
    * Bug ID
    * Requirement ID 
    * Description 
    * Name of Test Team leader
    * Expected results 
    * Name of developer who could fix it 
    * Name of developer who 
    * Logs 
    * Hardware
2. Attaching logs, traces, screenshot is an ……… activity 
    *	Mandatory 
    *	Optional 

3. Test contain 13 test steps. During testing you mark 12 steps as passed and 1 failed. Which status you will set to whole tests:
    *	Pass 
    *	Failed 
    *	Unknown 

4. Reporting a ticket to bug tracking system like Bugzilla is a task for: 
    *	Test Team leader 
    *	Developer 
    *	Test integrator 
    *	Unit Test Developer 
    *	Tester 
    *	Project Manager

5. Debugging is an activity done by: 
    *	Tester 
    *	Developer 
    *	Test Team Leader 
    *	Project Manager  

6. If you don’t how the tested system functionality should work correctly you will: 
    *	Ask team leader 
    *	Ask developer 
    *	Check it in google 
    *	Ask on popular online forum for testers
    *	Check in requirements 
    *	Do nothing 

7. You are testing mobile application for playing music. You receive test set with selected tests for this application. During performing test of pause/play functionality you discover that one per ten times functionality of pausing current played audio file doesn’t work correctly. Will you mark this tests as? 
    *	Pass 
    *	Failed 
    *	Unknown
    *	Skip this test

8. You are testing Bank System application which create monthly a summary in pdf format file. 
At the end of each month system send this summary as an attachment to each owner of account. 
During testing you discover that application send summary report in pdf file format but extension(“pdf”) of this attachment is missing. Of course you check that, and adding “.pdf” extension will present report.
What you will do as a first action: 
    *	Report  a bug 
    *	Report a Change request 
    *	Not report a bug
    *	Send an email to test team leader 
    *	Send an email to PM 

9. You are testing Bank System application. This application should allow user (after successful login) to check own amount of money and history of transaction on 4 most popular desktop browsers: IE, Firefox, Chrome and Opera. During testing you start Chrome browser on android devices and observe that user couldn’t log in to application. What you will do as a first action: 
    *	Report a bug 
    *	Not report a bug
    *	Send an email to test team leader 
    *	Send an email to PM
    *	Propose change request 
10. You are testing Bank System application. This application at 30 or 31(depend on month) of each months send monthly report. During testing you discover that per whole year you receive 11 reports.  What could goes wrong? 

11. You are testing multimedia system application. Every day you start from enabling collecting traces on tested system and perform assigned test sets.  After 4 hours of testing you find an issue in last performed test. It is a new issue so you decide to enter a bug. What you will do: 
    *	Attach all collected traces 
    *	Repeat last test again and collect new traces 
    *	Do not attach traces  
